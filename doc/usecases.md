users by country
  see dgoulet mail "New way of counting Direct Users per country" from 06.06.2016
    https://gitweb.torproject.org/metrics-web.git/tree/doc/users-q-and-a.txt
  karstens algorithm
    https://gitweb.torproject.org/metrics-web.git/tree/modules/clients/init-userstats.sql
    counting dirreq requests and extrapolating using bytes history
    for bridges see tech report counting-daily-bridge-users-2012-10-24.pdf
    for relays statistics are based on consensuses
  dgoulets algorithm
    only for relay, bridges are fine
    dirreq-stats-end
    dirreq-v3-reqs from extra-info


from karsten visualization sketches december 01.12.2015
  consensus download speeds 
    (timed out, < 1kbps, < 10, < 100, < 1 mbps, < 10, < 100)
  relays and the amount of traffic they handle
  exposed bad relays
    when they show up, when they are exposed, when they leave)
  software versions used
    (versus recommended)

    
phw
  sybilhunter 
    go https://gitweb.torproject.org/user/phw/sybilhunter.git/tree/
    python implementation in https://github.com/NullHypothesis/trnnr
    similarities between relays
      uses some clustering algorithms to detect showily pairwise similarities in
        fingerprints
        uptime
        bandwidth
        flags
        family
    ask him how this could be implemented in oniLyst


karstens importer for visionion
  calculating probabilities in src/Parse.java/calculatePathSelectionProbabilities
  looking up countries and ASs in src/Parse.java/lookUpCountriesAndASes
  parsing ports in src/Parse.java/formatPorts
  parsing OS in src/Parse.java/parseServerDescriptor
  etc etc
   

  https://metrics.torproject.org/bandwidth-data.html
  https://metrics.torproject.org/servers-data.html
  
  

# USECASES

### chasing bad relays
  
    [c] https://blog.torproject.org/blog/did-fbi-pay-university-attack-tor-users     
    [c] f. hat mich um 2 uhr morgens geweckt, kurz nachdem er den angriff entdeckt hat.
    [c] war gerade dev meeting in paris.
    [c] und er hat einen der relays beim angriff entdeckt.
    [c] jetzt ging es darum möglichst alles über diesen relay und ähnliche relays im netzwerk herauszufinden.
    [c] seit wann sind die da,
    [c] wie viele sind es,
    [c] welche ip-präfixe,
    [c] wie viele andere gibt es in diesen ip-präfixen.
    [c] wie schnell sind die, sind es exits,
    [c] contacts?
    [c] platforms?
    [c] versions?
    [c] alles.
    [c] angenommen f. hätte so eine datenbank gehabt,
    [c] was hätte er machen können?

### top 10 accessed graphs on metrics
 
    1. Direct users by country (17.92%)
    2. Bridge users by country (11.34%)
    3. Relays and bridges in the network (4.56%)
    4. Relays with Exit, Fast, Guard, Stable, and HSDir flags (3.74%)
    5. Total relay bandwidth in the network (3.29%)
    6. Unique .onion addresses (2.89%)
    7. Hidden-service traffic (2.46%)
    8. Bridge users by transport (2.33%)
    9. Relays by platform (1.83%)
    10. Relays by version (1.83%)
    
    https://webstats.torproject.org/out/meronense.torproject.org/

### trnnr - Tor relay nearest neighbour ranking
   https://github.com/NullHypothesis/trnnr 
   philip winter, 2.6.2016 on metrics-team
   To be able to leverage Stem's API, I reimplemented sybilhunter's nearest
   neighbour ranking in Python.  The tool takes as input a relay
   fingerprint, and then ranks all other ~7,000 relays by their similarity
   to the reference relay, most similar one first.  Relays are similar if
   they share information in their descriptor like ports, or their
   bandwidth values.
    The code currently resides on GitHub, and soon also in a Tor user
    repository: <https://github.com/NullHypothesis/trnnr>
    There's also a README file that talks about usage examples.
    
### :man könnte zum beispiel 

consensus-einträge und bridge-status-einträge in eine tabelle packen.
und dazu noch version-2-relay-network-status-einträge und version-1-relay-network-status-einträge.
das wäre vielleicht auch sinnvoll, um vergleiche über die letzten 10+ jahre anzustellen.
aber andere dinge passen da nicht wirklich gut rein.

:dann müssten wir uns etwas einfallen lassen wie wir consensuses und server-descriptors verbinden.
:allerdings riecht das auch wieder nach version 2.
:ein consensus-eintrag enthält einen server-descriptor-digest.
:und im server-descriptor stehen dann auch wieder interessante dinge.
:die infos sind quasi aufgeteilt.
:gleiches gilt für extra-info-descriptors, die jeweils zu genau einem server-descriptor gehören.
:ja, problem ist, dass sie aus drei quellen zusammengesetzt werden müsste.

:ok. mein vorschlag wäre mit consensuses anzufangen.
:oder!
:mit bridge-network-statuses.
:die sind nämlich chronisch unteranalysiert.
:und dann machen wir uns gedanken über bridge-server-descriptors und bridge-extra-infos.
:merk dir nur: schau nach bridges, nicht relays. :)

### visionion Usage Scenario

A user might just have some simple questions: how many relays were there running in the past 3 months in .de? How much bandwidth was provided by relays running Tor version 0.2.3.x.?
Such questions might just ask for a number and therefor need no visualization at all.
When a series of numbers is asked for or when information needs get more complex because they involve different factors and sources of data a visualization can be very handy to ease comprehension of the answer.
A simple graph of values over a period of time can already be grasped much more easily than a list of numbers.
Visually combining and contextualizing different aspects of data in one place can help understand causes, effects and correlations.
When there is a problem to which not only the right answer but also the right question is not known a visualization can help develop an understanding of what's going on in the first place.
Certainly this is an ambitious task for which the visualization has to provide interactive controls and visual malleablity.

How to achieve all this?
Basically, take all network graphs and merge them into one single graph with plenty of options.
Users should be able to navigate into any factor (bridge vs. relay, country, flags, Tor software version, operating system, EC2 cloud bridge or not) and learn the total relay number or advertised bandwidth or bandwidth history or any other available metric for their selection.

**Scenario 1 : a classic curve**
The most prominent usecase is the timeline with a graph representing volumina of bandwidth or number of hosts or number of clients etc. on the vertical axis.
This scenario is currently implemented by the Tor metrics project [graph visualizations](https://metrics.torproject.org/network.html).

**Scenario 1a : curves layered on each other**
It should also be possible to layer timeline graphs for the same time period but with different subject on each other to compare eg correlations between consumed bandwidth and number of clients.
This scenario is currently available as a prototype at [interactive graphs](http://tigerpa.ws/tor_metrics/).

**Scenario 2 : adding detail on an orthogonal plane**
Imagine a plane orthogonal to the graph, representing some other data at that point in time eg adding to the graph of Linux driven relays a cake diagram of all operating systems driving relays.
This is one step beyond simple layering of graphs. Combining different visualization techniques like timeline and cake diagram visualizes relations between different categories of data. Control shifts from the visualization framework to the web application.

**Scenario 3 : geography**
A ground plane on the floor might show geographic distribution of linux driven relays and how much bandwidth each of them handles, the imaginary center of Linux driven traffic at the crosspoint of the first two planes.
The geographical dimension is not very strongly represented in the raw data but nonetheless sometimes an important perspective.

**Scenario 4 : events**
Now add markers for certain events: the day when traffic from linux driven relays peaked, the day it hit an alltime low, the days it plummeted, the days it spiked etc.
Markers point the user in directions that might be worth to explore. Its driven by some analytics in the background.

**Scenario 5 : amassments**
Emphasize the biggest nodes for a given metric and their share of the total.
Facilitate checks for (de-) centralizations in the infrastructure.

More usecases:

* Visualizing the total pbr of all relays with a certain characteristic, e.g. what's the total pbr of all relays in Germany?
* number of relays running version 0.2.4 on Linux.
* bandwidth provided by relays running version 0.2.4
* bytes per day transported by relays running Linux.
* number of relays running with type Guard and Middle, with the Fast and Stable flag, with version 0.2.3.x, on OS X, in AS 1234, not permitting any ports, in Germany, on May 23, 2013   (maximum level of detail)
* total pbr of all relays with a certain characteristic. E.g. what's the total pbr of all relays in Germany?
* bwa, bwc, and pbr are more important measures than the number of nodes.


# MISSING DATA

### nima's bwauth

    Nima Fatemi <nima@torproject.org>,  Tue, 4 Oct 2016 22:20:04 +0200 (CEST)
    [...] I'm running a BW Auth in
    HK. If you run a DA and trust me enough to pair, please let me know. I
    can make the results available over a webserver if needed. Otherwise, I
    think I will shut this scanner down by the end of the year.
    Till then, you can always find the latest scan results and measured
    percentage here:
    https://raw.githubusercontent.com/mrphs/bwauth/master/latest_bwscan
    https://raw.githubusercontent.com/mrphs/bwauth/master/measured_percentage.txt

### dgoulet    
    
    David Goulet <dgoulet@ev0ke.net>, Tue, 30 Aug 2016 15:16:58 +0200 (CEST)
    On 30 Aug (10:04:03), Karsten Loesing wrote:
    | [...] Is there anybody in the community team (or another friendly volunteer
    | on this list) who'd be comfortable writing a small Python script that
    | fetches Onionoo data [1] and filters out contact information of relays
    | matching given criteria?
    I do have lots of scripts that interacts with CollecTor and uses stem to parse
    it all. I've used Onionoo few times but with bash scripts and grep/awk magic.
    [...] I already have so many scripts and cron running to detect bad relays on 
    our network and measure the network[1]
    [...]
    [1] http://ygzf7uqcusp4ayjs.onion/tor-health/tor-health/index.html

    
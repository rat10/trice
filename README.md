#trice

##analytics suite

**trice** is an analytics suite for Tor metrics data. It wants to lower the entry barrier for analytics work on Tor data by reducing the technical skills and upfront fuzz and effort required until now.  
Proceed with caution, as **trice** is still *work in progress* (check out the immense [TODO](TODO.md) list).

**trice** is composed of the following modules:
* [**trans**](https://gitlab.com/rat10/trans/) - **conversion** to Parquet and JSON, from CollecTor
* [**truck**](https://gitlab.com/rat10/truck/) - **storage** solutions, optimized for analytics
* [**treat**](https://gitlab.com/rat10/treat/) - **aggregation** pipelines, bits and pieces
* [**trope**](https://gitlab.com/rat10/trope/) - **visualization** tools, web based

And these modules come in three different flavours:
* [**lean**](doc/triceLean.md) - check a thing or two in the data, with minimal fuzz upfront
* [**base**](doc/triceBase.md) - for regular analytics work, if PostgreSQL is too slow
* [**full**](doc/triceFull.md) - every hotness the analytics world has to offer, easy scale out


##Introduction

[The Tor Project](https://www.torproject.org/) network aims to provide means for anonymous usage of the internet to everybody, everywhere. The Tor [metrics project](https://metrics.torproject.org/) collects data about the Tor network - availability, usage, functionality - in ways that don't jeopardize the primary goal of anonymization. From this data insights can be derived about performance bottlenecks, possible attacks on anonymization, opportunities for optimization etc. See the [**usecases**](doc/usecases.md) for an idea of what analytic jobs there are.

The data is publicly available through the [CollecTor](https://collector.torproject.org/) service, ready for download in hourly, daily or monthly chunks. It is mostly unaggregated, fine grained raw data - per server, per hour - and serialized in a special, homegrown format, that is optimized for conciseness and compressability, trying to keep the burden of collecting this data on the network as low as possible. Since they use a homegrown serialization these data files need dedicated libraries for programmatic access. [metrics-lib](https://gitweb.torproject.org/metrics-lib.git) for Java is the most complete one and maintained by the Tor Project itself, but there exist also [others](https://metrics.torproject.org/development.html#libraries) for Python, Go etc. Non-programmatic access is possible but generally makes no sense: the data is just too fine grained and too copious to sift through it by hand.

The entry hurdle to analyze the Tor metrics data is therefor quite high: at least one has to be able to set up a development environment with one of the dedicated libraries to extract the desired bits of data from the CollecTor files - a non-programmer will already be out of luck. Since it's a lot of data also a programmer will have to choose the right tools and make some non-trivial design choices to build a performant system that doesn't bog down a local machine for hours or days and eat up the harddrive on the way. Last not least not every information aspect is readily available in the raw data but instead might require considerable transformations and aggregations upfront before the data is in a state that allows to ask the questions one is really interested in.  


##modules

The **trice** analytics suite is organized into a set of modules. 

###trans - conversion

The converter, [**trans**](https://gitlab.com/rat10/trans/), converts raw data from Tor's serialization used in CollecTor to JSON and Parquet serializations. JSON can be used within every modern web application framework and with a lot of end user tools like the popular BI visualization tool Tableau. Parquet is a columnar storage format that's optimized for analytic work and well supported in Big Data analytics tools like [Spark](https://spark.apache.org/) and [Drill](https://drill.apache.org/). **Trans** in a pretty good shape already and can be used right away. It still lacks a test suite tough, so it most definitely contains some bugs. Be prudent and double check your findings!
Also the **trans** converters are as faithful to CollecTors schema as possible to make them easily usable for those that are already familiar with the Tor spec. Unfortunately that serialization contains a lot of pretty deeply nested data structures that are hard to handle in analytic tools. Therefor a second set of converters is planned that shall generate a much flatter result optimized for consumption by Apache Spark and similar tools.


### truck - storage

Analytic workloads require specific storage solutions and [**truck**](https://gitlab.com/rat10/truck/) offers 3 variations tailored towards specific requirements. In the most general usecase an one-off question comes up that a quick run over some data can answer: then there's no need for a specialized storage solution and combinations of data converted to JSON files and analyzed in Tableau or data converted to Parquet files and aggregated in Spark will do the job. OTOH continuous observations or complex explorations of the network can't be efficiently run from file based data storage but need database setups optimized for analytic workloads and capable of efficiently handling updates.  
**truck** supports two options: [SnappyData](http://www.snappydata.io/) and [Greenplum](http://greenplum.org/). Both databases are optimzed for analytic workloads through 'columnar' storage, but target different audiences: SnappyData is build around [Spark](https://spark.apache.org/), the popular Big Data analytics engine, providing interfaces to Java, Python, Scala and R and easy scalability from notebook to cloud, whereas Greenplum is a fork of PostgreSQL, enhanced through an advanced query optimizer, the aformentioned columnar storage and support for clustered setups, thereby providing Big Data performance with a familiar PostgreSQL interface.  
While the concept of **truck** seems clear and sound the implementation progress currently suffers from some pesky details. 


###treat - aggregation

With [**treat**](https://gitlab.com/rat10/treat/) some elementary aggregation scripts will be provided, concentrating on identifying the common basic building blocks of Tor analytic pipelines and assembling a ready to use set of aggregation scripts. They also show recurring coding idioms and may serve as starting blocks for more specialized and detailed analytics. Since the different **truck** storage solutions support overlapping sets of languages and interfaces, aggregations developed for a local Spark installation with Parquet files should work with all but small modifications also on a distributed SnappyData cluster or, in the SQL and R variations, on Greenplum.


###trope - visualization
 
The visualization part, [**trope**](https://gitlab.com/rat10/trope/), will show how to make the computed insights and analyses accessible in shared interactive visualizations.**Trope** started as it's own project, [visionion](http://www.github.com/tomlurge/visionion), a few years ago, aiming for a comprehensive tool to visually analyze Tor data, but got bogged down by the complexity of the data structures and upfront aggregation tasks. Also in the meantime visualization tools like Tableau (although closed source) and [Apache Zeppelin](https://zeppelin.apache.org/) surfaced that are broadly applicable and feature rich. Therefor the focus changed from developing a complete visualization solution to setting up a working environment where existing visualization tools fit in nicely. In that respect [Spark](https://spark.apache.org/), [SnappyData](http://www.snappydata.io/) and [Zeppelin](https://zeppelin.apache.org/) are a very promising constellation.


##flavours TODO

###the complexity of dealing with Tor metrics data TODO

Any analytisc setup for Tor metrics data has to deal with three problems: 
* the total volume of data is massive, 
* the data model is deeply nested and 
* frequent updates to already published data. 
Depending on nature of the analytics task different solutions have to be chosen.

A one-time check of a specific assumption doesn't require setting up a comprehensive environment that handles updates, scales out to the cloud etc. Converting Tor data to Parquet or JSON files and working on them with the analytics tool of your choice can get you a long way. *triceLean* is one such option and being based on Parquet serialization and Apache Spark analytics engine it is upwards compatible with *triceFull*. Other options are JSON and some BI tool like Tableau or further aggregations with MongoDB. 
Directly working from converted files is a quick and convinient way to get started with analytics and it can be extremly fast too. JSON has its merits in ubiquitious support, but Parquet is a columnar storage format optimized for Big Data analytics. Parquet is very performant and normally faster than any analytics database. 
Of course files do have disadvantages too the most important being that updated data or turning towards a longer timespan will need conversion of a new file. Also Spark cannot treat a directory of files as logically one file (Apache Drill, a SQL engine, can do that with Parquet files). Joins over seperate files are possible though.

When analytics need to be performed on a regular basis and with the latest metrics data (and updates - corrections, additions - to already published data) file based storage is not enough: a database is needed and *triceBase* and *triceFull* come into play.
If you started out with a filebased solution as in *triceLean* you will find a reasonable upgrade path in switching to *triceFull*, allowing you to reuse your aggregation scripts because both solutions are based on the same analytics engine, Apache Spark. *triceFull* also integrates with the latest and greatest analytics libraries around, plus it scales very well - both locally and to the cloud.
Otherwise *triceBase* has much less bells & whistles than *triceFull* but provides a very robust environment that will in most cases be sufficient. It's based on PostgreSQL so will feel quite familiar to people with that background. The downside is that aggregations developed for *triceLean*/Spark will have to be adapted to *triceBase*. Although they all work with SQL there are differences. 
TODO is that true? what if lean worked with the flattened version?

Not the least problem with Tor metrics data is the deeply nested data model. Analytics data is generally modeled as flat as possible because that allows for efficient runs over a specific field through large numbers of records. Tor metrics OTOH data is optimized for space efficient transfer with low load on the network. There is again no one-size-fit-all solution to this problem and no obvious way how to denormalize the Tor data model. **trice** offers two different approaches: **trans** conversions can either be as faithfull as possible to the original data format or they can be only slightly nested - one level deep at most - to make them easier and faster to query. If you are already familiar with the Tor data model you'll feel right at home with the faithful conversion. If you want to aggregate over more than one type of descriptors and/or need to consult the documentation for the fieldtypes anyway the flattened version is your friend. Both conversion types work well with *triceLean*. *triceBase* will benefit from the flattened concersion whereas the faithful, nested conversion caters better to the transactional backend of *triceFull*.


###usecases / possible scenarios TODO

* if you want to have an easy start use truckLean (spark standalone).  
* if you want to upgrade later consider truckFull (snappydata) and reuse your spark scripts.  
* if you plan to use analytics regularily but do not need the most scalable solution or the newest hottness probably truckBase (greenplum) is a good solution.  
* if you want to be able to scale out to the cloud easily, use the latest analytics tools and libraries and need to interactively drill into the data go for truckFull
	
data conversion in **trans** needs to be configured depending on the truck chosen.   
discussion of file sizes (here?) -> 1 month of votes = 3 gb parquet = massive!  
more emphasis on commenting out unneded fields in conversion

* truckLean with Spark requires the whole timespan in one Parquet file.  
these files can get prohibitively large so you need to consider commenting out unneeded fields. see the trans documentation for how to do this.
* truckFull can work from files just like truckLean but works best if the data is first imported into the Snappy row store. use monthly data files per descriptor for this  
(TODO - really? or better the "flat" version? this has to be worked out in **truck**)
* truckBase  - same as Snappy, i suppose?



##History

During the Tor Developer meeting in Berlin, automn 2015 the idea came up that it would be good to have all measurement data on one server, loaded into a database, with a suite of analytics software readily set up, available to everybody who needs to do aggregation, analytics, research etc on the data. 

Right now everybody who wants to analyze Tor metrics data first has to downlod raw data from the metrics website, load it into a local database, massage it, setup a query environment etc. That can be cumbersome and use siginificant computing resources, maybe making one's notebook unusable for hours or eating up hundreds of gigabytes of disk space if not optimized properly. That makes on the fly quick and dirty prototyping and analysis quite unfeasable and prohibitively tedious.

The initial idea was to set up an analytics server free for everybody to use, containing all the raw metrics data available, stored in a HBase database on Hadoop/HDFS, with analytic tools like Spark and Drill on top, supporting diverse languages - SQL, R, Python, Java, Scala, Clojure - and with gateways to desktop-ish tools like Tableau and MongoDB. 

<img src="docs/32c3/slide.png">   

This slide is from the [State of the Onion](https://media.ccc.de/v/32c3-7307-state_of_the_onion) talk at 32c3 in december 2015. It captures the basic idea quite nicely, but nonetheless some of the details are outdated already (including the name). Also while the effort was then part of the Tor metrics team it is not anymore. It is now an individual initiative. As a consequence the plan for a dedicated server has been dropped and the 'analytics project' has been renamed to **trice** and rethought as a suite of tools for setting up an analytics environment on an normal laptop plus the ability to scale it out to the cloud easily when (and only as long as) need be. That will probably be sufficient for a vast share of usecases. Working with *all* available data at once - uncompressed about 1 TB of raw data - in 2017 still needs a rather beefy machine 'though.




 

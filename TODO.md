docker/vagrant support bei snappydata + greenplum checken
shell skripte 
	zum ziehen neuer descriptoren
	zum starten entsprechender 
		konvertierungen
		db imports
		aggregationen
konverter
	flattened
	variable timespans
aggregations skripte
	basic aggregations
	aggregations of aggregations
	complex aggregations
		user estimates
		visionion
database
	import scripts
dynamic analytics - on the fly analytics auf den aggregationen und der datenbank
	testcase ausprobieren
	spark ML
	spark Graph
	spark + elastic search
	interface?
visualisierung
	im web
		d3
		cristobals demo with backbone
		react + scalascript
	business intelligence tools
		apache zeppelin
		rstudio
		tableau
parallelisierung
	übertragung der lokalen installation in die cloud	
	abarbeitung einzelner jobs in der cloud
		datenbereithaltung auf S3 oder ähnlichem?


reformulierung des konzepts
	aufgaben
		streaming ingestion
		triggers 
		aggregations
		visual interface
		accessabilty
	snappyData
		integration von db und spark, ohne die vorteile von spark aufzugeben
			kann ohne große verluste durch ausgefeiltere lambda architektur ersetzt werden
		aqp verbessert response ziet und interaktive suche
		ec2 connector erlaubt zb auslagerung eines großen auftrags in die cloud
			zb 	grosses update der datenbasis
				alarmmässige suche nach einem loch im netz
		zeppelin
		jdbc/odbc + scala/spark interfaces